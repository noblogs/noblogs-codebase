<?php

// Common settings
$wpdb->persistent = true;
$wpdb->max_connections = 30;

/* Disable responsiveness check, is not needed in our configuration and is causing 
 connection errors for the wrong implementation */
$wpdb->check_tcp_responsiveness = false;

include_once('../r2db/db-hash.php');
include_once('../r2db/db-backends.php');

// Set up global dataset with master databases
noblogs_load_global_dataset(NOBLOGS_MASTER_CONFIG, NOBLOGS_HOST_FILE);

// Add all the sharded blog databases.
$wpdb_reverse_backend_map = noblogs_load_backends(NOBLOGS_BACKEND_CONFIG, $wpdb->hash_map);
