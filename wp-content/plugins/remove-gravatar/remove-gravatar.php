<?php
/*
Plugin Name: Wordpress-privacy-ai
Version: 0.0.1
Plugin URI: none
Description: Filtra alcuni parametri per la privacy
Author: A/I
*/

//Elimino i gravatar in caso di avatar non presente
if ( !function_exists('get_avatar') ) :
function get_avatar( $id_or_email, $size = '96', $default = '', $alt = false ) {
        if ( ! get_option('show_avatars') )
                return false;

        if ( false === $alt)
                $safe_alt = '';
        else
                $safe_alt = esc_attr( $alt );

        if ( !is_numeric($size) )
                $size = '96';

        $email = '';
        if ( is_numeric($id_or_email) ) {
                $id = (int) $id_or_email;
                $user = get_userdata($id);
                if ( $user )
                        $email = $user->user_email;
        } elseif ( is_object($id_or_email) ) {
                // No avatar for pingbacks or trackbacks
                $allowed_comment_types = apply_filters( 'get_avatar_comment_types', array( 'comment' ) );
                if ( ! empty( $id_or_email->comment_type ) && ! in_array( $id_or_email->comment_type, (array) $allowed_comment_types ) )
                        return false;

                if ( !empty($id_or_email->user_id) ) {
                        $id = (int) $id_or_email->user_id;
                        $user = get_userdata($id);
                        if ( $user)
                                $email = $user->user_email;
                } elseif ( !empty($id_or_email->comment_author_email) ) {
                        $email = $id_or_email->comment_author_email;
                }
        } else {
                $email = $id_or_email;
        }

        if ( empty($default) ) {
                        $default = 'mystery';
        }


        if ( 'mystery' == $default )
                $default = plugins_url( 'images/mystery-man-50.jpg' , __FILE__ );
        elseif ( 'blank' == $default )
                $default = includes_url('images/blank.gif');
                $avatar = "<img alt='{$safe_alt}' src='{$default}' class='avatar' height='{$size}' width='{$size}' />";
        return apply_filters('get_avatar', $avatar, $id_or_email, 40 , $default, $alt);
}
endif;
