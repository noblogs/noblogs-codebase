<?php get_header();

if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class('img-attachment'); ?>>

<h2><?php the_title(); ?></h2>
<ul class="meta">
<li><?php edit_post_link(); ?></li>
</ul>

<div class="main-img"><a class="thickbox" title="<?php the_title(); ?> <?php  _e('(press ESC to close)','zenlite');?>" href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'large' ); ?></a></div>

<div class="postcontent wp-caption-text">
<?php the_excerpt(); ?>
</div>

<?php
$args = array(
	'post_type' => 'attachment',
	'post_mime_type' => 'image',
	'numberposts' => -1,
	'post_status' => null,
	'post_parent' => $post->post_parent
	);
$images = get_posts($args);
if( count($images) > 1 ) :?>
<h3 class="more-images"><?php _e('More images posted under', 'zenlite');?> <a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment"><?php echo get_the_title($post->post_parent); ?></a></h3>
<ul class="prevnext image-nav">
<li class="prev_img"><?php previous_image_link(); ?></li>
<li class="next_img"><?php next_image_link();?></li>
</ul>

<?php else:?>
<p class="posted-under"><?php _e('Posted under', 'zenlite');?> <a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment"><?php echo get_the_title($post->post_parent); ?></a></p>
<?php endif;?>

 </div>

<?php endwhile; endif;
get_footer();