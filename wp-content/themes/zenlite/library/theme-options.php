<?php

// Load theme options
require_once get_template_directory() . '/library/zenlite-theme-options-array.php';

// White list theme options
function zenlite_options_init() {
	register_setting( 'zenlite_options', 'zenlite_theme_options', 'zenlite_options_validate' );
}
add_action( 'admin_init', 'zenlite_options_init' );

// Add theme options page
function zenlite_options_add_page() {
	$theme_page = add_theme_page( __( 'Theme Options', 'zenlite' ), __( 'Theme Options', 'zenlite' ), 'edit_theme_options', 'theme_options', 'zenlite_options_do_page' );
	if ( $theme_page ) add_action( 'load-' . $theme_page, 'add_zenlite_contextual_help' );
}
add_action('admin_menu', 'zenlite_options_add_page');

// Add basic contextual help links
function add_zenlite_contextual_help() {
	$screen = get_current_screen();
	$help = '<ul class="contextual-links">';
	$help .= '<li><a href="http://quirm.net/forum/forum.php?id=15">' .__( 'ZenLite support on Quirm.net', 'zenlite') . '</a></li>';
	$help .= '<li><a href="http://wordpress.org/tags/zenlite">' .__( 'ZenLite support on wordress.org', 'zenlite') . '</a></li>';
	$help .= '</ul>';
	$screen->add_help_tab( array(
		'id'      => 'theme-support',
		'title'   => __( 'Theme Support', 'zenlite' ),
		'content' => $help,
	) );
}

function zenlite_enqueue_options_style() {
	wp_enqueue_style('theme_options_style', get_template_directory_uri() . '/library/theme-options.css', '', false, 'screen');
	if ( file_exists( get_stylesheet_directory() . '/theme-options-custom.css' ) )
		wp_enqueue_style('theme_options_custom_style', get_stylesheet_directory_uri() . '/theme-options-custom.css', '', false, 'screen');
}
add_action('admin_print_styles-appearance_page_theme_options', 'zenlite_enqueue_options_style');

// Create tabs
function zenlite_option_tabs( $current = 'options' ) {
    $tabs = array( 'options' => 'Options', 'guide' => 'Options Guide', 'post-formats' => 'Post Formats' );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) $active = ' nav-tab-active';
        else $active = '';
        $links[] = '<a class="nav-tab' . $active . '" href="?page=theme_options&amp;tab=' . $tab . '">' . $name . '</a>';
    endforeach;
    echo '<ul class="nav-tabs">';
    foreach ( $links as $link ) echo '<li>' . $link . '</li>';
    echo '</ul>';
}

// Theme options form
function zenlite_options_do_page() {
	global  $pagenow;
	$updated = false;
	if ( isset( $_REQUEST['updated'] ) || isset( $_REQUEST['settings-updated'] ) ) $updated = true;?>
	<div class="wrap">
	<?php screen_icon(); echo "<h2>" . wp_get_theme() . "</h2>"; ?>
	<?php if ( $updated == true ) : ?>
	<div class="updated fade"><p><strong><?php _e( 'Your theme options have been saved', 'zenlite'); ?></strong></p></div>
	<?php endif; ?>

	<?php include_once 'donate.php';
	if( $pagenow == 'themes.php' && $_GET['page'] == 'theme_options' ) :
		if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
		else $tab = 'options';
		switch ( $tab ) {
			case 'options' :
			zenlite_option_tabs($current= 'options');
			zenite_do_options_tab();
			break;

			case 'guide' :
			zenlite_option_tabs($current= 'guide');
			zenlite_do_guide_tab();
			break;

			case 'post-formats' :
			zenlite_option_tabs($current= 'post-formats');
			zenlite_do_post_format_tab();
			break;

		}
	endif;
}

// Do guide tab
function zenlite_do_guide_tab() {
	include_once get_template_directory() . '/library/zenlite-options-guide.php';
}

// Do general tab
function zenlite_do_post_format_tab() {
	include_once get_template_directory() . '/library/zenlite_post_formats.php';
}

// Do options tab
function zenite_do_options_tab() {
	global  $zenlite_all_theme_options;
	?>
	<form method="post" action="options.php" id="<?php echo 'zenlite';?>_options"><div>
	<?php settings_fields( 'zenlite' . '_options' ); ?>
	<?php $stored_options = get_option('zenlite' . '_theme_options');
	?>

	<fieldset>
	<legend><?php _e('Use the non-custom navigation menu to display links to:','zenlite'); ?></legend>
	<?php $option_name = 'menu_type';
	zenlite_options_sort( $zenlite_all_theme_options[$option_name], $stored_options, $option_name);?>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display site name &amp; tagline above header image?','zenlite'); ?></legend>
	<?php $option_name = 'header_text';
	zenlite_options_sort( $zenlite_all_theme_options[$option_name], $stored_options, $option_name);?>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display "Pages in this section" list?','zenlite'); ?></legend>
	<?php $option_name = 'pagetree';
	zenlite_options_sort( $zenlite_all_theme_options[$option_name], $stored_options, $option_name);?>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display auto-generated title on Posts/Pages without titles?','zenlite'); ?></legend>
	<?php $option_name = 'notitle_display';
	zenlite_options_sort( $zenlite_all_theme_options[$option_name], $stored_options, $option_name);?>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display author name/link on Posts?','zenlite'); ?></legend>
	<?php $option_name = 'author_display';
	zenlite_options_sort( $zenlite_all_theme_options[$option_name], $stored_options, $option_name);?>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display allowed tags on comment form?','zenlite'); ?></legend>
	<?php $option_name = 'kses_display';
	zenlite_options_sort( $zenlite_all_theme_options[$option_name], $stored_options, $option_name);?>
	</fieldset>

	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Update Theme', 'zenlite') ?>" /></p>

	</div></form>
</div>
	<?php
}

// sort inputs for display
function zenlite_options_sort($option_input, $stored_options, $name) {
	switch ($option_input['type']) {
		case 'radio':
		zenlite_options_do_radio($option_input['options'], $stored_options, $name, $option_input['default']);
		break;

		default:
		break;
	}
}

// Radio button display
function zenlite_options_do_radio($radio_options, $stored_options, $name, $default) {
	if ( ! isset( $checked ) ) $checked = '';
	foreach ( $radio_options as $option ) {
		if ( isset( $stored_options[$name] ) ) {
			if( $stored_options[$name] == $option['value'] ) $checked = 'checked="checked"';
			else $checked = '';
		}
		else{
			if ( $option['value']  ==  $default ) $checked = 'checked="checked"';
			else $checked = '';
		}?>
		<label class="description" for="<?php echo $name;?>_<?php echo esc_attr( $option['value'] ); ?>"><input type="radio" id="<?php echo $name;?>_<?php echo esc_attr( $option['value'] ); ?>" name="<?php echo 'zenlite';?>_theme_options[<?php echo $name;?>]" value="<?php echo esc_attr( $option['value'] ); ?>" <?php echo $checked; ?> /> <?php echo $option['label']; ?></label>
		<?php
	}
}

// Sanitize and validate inputs
function zenlite_options_validate( $input ) {
	$new_input = array();
	global $zenlite_all_theme_options;
	$theme_option_names = array_keys($zenlite_all_theme_options);

	foreach($theme_option_names as $theme_option_name) {
		$option_type = $theme_option_name['type'];
		switch ($option_type) {
			case 'checkbox':
			$new_input[$theme_option_name] = ( $input[$theme_option_name] == 1 ? 1 : 0 );
			break;

			case 'text':
			$new_input[$theme_option_name] = wp_filter_nohtml_kses( $input[$theme_option_name] );
			break;

			case 'textarea':
			$new_input[$theme_option_name] = wp_filter_post_kses( $input[$theme_option_name] );
			break;

			default:
			$new_input[$theme_option_name] = $input[$theme_option_name];
		}
		if( !isset( $input[$theme_option_name] ) ) $new_input[$theme_option_name] = $theme_option_name['default'];
	}
	return $new_input;
}
