<?php get_header();

if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class();?>id="post-<?php the_ID();?>">

<?php
$format = get_post_format();
if ( false === $format ) $format = 'standard';
get_template_part( 'format', $format );?>

</div>
<?php comments_template();?>

<?php endwhile; ?>

<?php  endif;
get_footer();