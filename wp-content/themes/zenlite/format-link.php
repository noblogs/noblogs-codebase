<?php
// Get the text & url from the first link in the content
$content = get_the_content();
$link_string = zenlite_extract_from_string('<a href=', '/a>', $content);
$link_bits = explode('"', $link_string);
foreach( $link_bits as $bit ) {
	if( substr($bit, 0, 1) == '>') $link_text = substr($bit, 1, strlen($bit)-2);
	if( substr($bit, 0, 4) == 'http') $link_url = $bit;
}?>

<h2 class="post-title"><a href="<?php echo $link_url;?>" title="<?php _e('External link', 'zenlite');?>"><?php echo $link_text;?></a></h2>

<?php if( !is_search() ) :?>
<ul class="meta posthead">
<li><?php edit_post_link(sprintf( __('Edit %1$s', 'zenlite'), get_the_title() )
); ?></li>
</ul>
<?php endif;?>

<?php if( is_single() ) :?>
<div class="postcontent">
<?php if( isset( $link_url ) ) : ?><p><a href="<?php echo $link_url;?>"><?php echo $link_url;?></a></p>
<?php else: the_content();endif;?>
</div>
<?php endif;?>

<?php if( !is_search() ) :?>
<ul class="meta postfoot">

<li><?php _e('Posted on ', 'zenlite'); the_time(get_option('date_format')); the_time(get_option('time_format')); echo zenlite_author_display(__(' by ', 'zenlite'), '')?></li>

<?php if( !is_single() && 'open' == $post->comment_status) : ?>
<li class="comment-link"><?php comments_popup_link(
__('Comment on ', 'zenlite') . get_the_title($id),
__('1 Comment on ', 'zenlite') . get_the_title($id),
 __('% Comments on ', 'zenlite') . get_the_title($id),
'postcomment',
__('Comments are off for ', 'zenlite') . get_the_title($id)
); ?> &raquo;</li>
<?php endif;?>

<?php if(!is_category() && get_the_category() ) :?><li class="cats"><?php _e('Filed under:', 'zenlite');?> <ul><li><?php the_category(',</li> <li>') ?></li></ul></li><?php endif;?>

<?php if( !is_tag() && get_the_tag_list() ) :?><li class="tags"><?php _e('Tags:', 'zenlite');?> <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>

</ul>

<?php endif;
comments_template();