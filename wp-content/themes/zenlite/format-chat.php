<h2 class="post-title"><?php if( !is_single() ) :?><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php endif;?><?php the_title();?><?php if( !is_single() ) :?></a><?php endif;?></h2>

<?php if( !is_search() ) :?>
<ul class="meta posthead">
<li><?php edit_post_link(sprintf( __('Edit %1$s', 'zenlite'), get_the_title() )
); ?></li>
</ul>
<?php endif;?>

<?php if( !is_search() ) :?>
<div class="postcontent">
<?php the_content('', false,'');?>
</div>
<?php endif;?>

<?php  if( is_single() ) :
$args = array(
	'before' => '<div class="pagelist">' . __('Pages:', 'zenlite'),
	'after' => '</div>',
	'link_before' => '',
	'link_after' => '',
	'pagelink' => '%'
);
wp_link_pages($args);
endif;?>

<?php if( !is_search() ) :?>
<ul class="meta postfoot">

<?php if( !is_single() ) :?><li class="more-link"><a href="<?php the_permalink();?>#more-<?php echo $post->ID;?>"><?php _e('Continue reading ', 'zenlite');the_title();?></a></li><?php endif;?>

<li class="added-by"><?php echo zenlite_author_display(__('Added by ', 'zenlite'), __( ' on ', 'zenlite'));the_time(get_option('date_format')); ?> <?php the_time(get_option('time_format'));?></li>

<?php if( !is_single() && 'open' == $post->comment_status) : ?>
<li class="comment-link"><?php comments_popup_link(
__('Comment on ', 'zenlite') . get_the_title($id),
__('1 Comment on ', 'zenlite') . get_the_title($id),
 __('% Comments on ', 'zenlite') . get_the_title($id),
'postcomment',
__('Comments are off for ', 'zenlite') . get_the_title($id)
); ?> &raquo;</li>
<?php endif;?>

<?php if(!is_category() && get_the_category() ) :?><li class="cats"><?php _e('Filed under:', 'zenlite');?> <ul><li><?php the_category(',</li> <li>') ?></li></ul></li><?php endif;?>

<?php if( !is_tag() && get_the_tag_list() ) :?><li class="tags"><?php _e('Tags:', 'zenlite');?> <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>
</ul>

<?php endif;
comments_template();