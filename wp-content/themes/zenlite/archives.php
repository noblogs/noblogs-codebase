<?php
/*
Template Name: Archive Lists
*/

get_header(); ?>

<h2><?php _e('Archives by Month:', 'zenlite');?></h2>
<ul>
<?php wp_get_archives('type=monthly'); ?>
</ul>

<h2><?php _e('Archives by Subject:', 'zenlite');?></h2>
<ul>
<?php wp_list_categories(); ?>
</ul>

<?php get_footer();