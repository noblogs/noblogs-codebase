<h2 class="post-title"><?php if( !is_single() ) :?><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php endif;?><?php the_title();?><?php if( !is_single() ) :?></a><?php endif;?></h2>

<?php if( !is_search() ) :?>
<ul class="meta posthead">
<li><?php edit_post_link(sprintf( __('Edit %1$s', 'zenlite'), get_the_title() )
); ?></li>
</ul>
<?php endif;?>

<?php if( !is_single() && !is_search() ) :?><div class="more-link"><a href="<?php the_permalink();?>"  title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php if(has_post_thumbnail() ) : the_post_thumbnail(); else : ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/image.png" width="100" height="100" alt="" /><?php endif;?> <?php printf( __('View %1$s', 'zenlite'), get_the_title() );?></a></div>

<?php else:?>
<div class="postcontent">
<?php if( !is_search() ) :
$args = array(
	'post_type' => 'attachment',
	'post_mime_type' => 'image',
	'posts_per_page' => -1,
	'post_status' => null,
	'post_parent' => $post->ID
	);
$images = get_posts($args);
if( $images ) : foreach( $images as $image) :?>
<div class="main-img"><a class="thickbox" title="<?php the_title(); ?> <?php  _e('(press ESC to close)','zenlite');?>" href="<?php echo wp_get_attachment_url($image->ID); ?>"><?php echo wp_get_attachment_image( $image->ID, 'large' ); ?></a></div>
<?php endforeach;
else: the_content();
endif;
else : the_excerpt();
endif; ?>
</div>

<?php endif;?>

<?php if( !is_search() ) :?>
<ul class="meta postfoot">

<li class="published-on"><?php _e('Published on ', 'zenlite'); the_time(get_option('date_format')); the_time(get_option('time_format')); echo zenlite_author_display(__(' by ', 'zenlite'), '')?></li>

<?php if( !is_single() && 'open' == $post->comment_status) : ?>
<li class="comment-link"><?php comments_popup_link(
__('Comment on ', 'zenlite') . get_the_title($id),
__('1 Comment on ', 'zenlite') . get_the_title($id),
 __('% Comments on ', 'zenlite') . get_the_title($id),
'postcomment',
__('Comments are off for ', 'zenlite') . get_the_title($id)
); ?> &raquo;</li>
<?php endif;?>

<?php if(!is_category() && get_the_category() ) :?><li class="cats"><?php _e('Filed under:', 'zenlite');?> <ul><li><?php the_category(',</li> <li>') ?></li></ul></li><?php endif;?>

<?php if( !is_tag() && get_the_tag_list() ) :?><li class="tags"><?php _e('Tags:', 'zenlite');?> <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>

</ul>

<?php endif;
comments_template();