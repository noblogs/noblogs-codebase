<!-- end content -->
</div>

<div id="footer">

<?php if( is_active_sidebar( 'widget-footer' ) ) :?><div class="widget-area"><?php endif;
dynamic_sidebar( 'Footer' );
if( is_active_sidebar( 'widget-footer' ) ) :?></div><?php endif;?>

<ul class="site-links">
<li class="top"><a href="#top" title="Jump to the top of this page">Top</a></li>
<li class="loginout"><?php wp_loginout(); ?></li>
<?php wp_register(); ?>
<li class="rss"><a href="<?php bloginfo('rss2_url'); ?>" title="RSS Feed"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/rss.png" width="14" height="14" alt="RSS" /></a></li>
</ul>

<p class="credits"><?php printf( __('Powered by the %1$s Theme', 'zenlite'),  wp_get_theme() );?></p>
</div>

<!-- end wrapper -->
</div>

<?php wp_footer(); ?>
</body>
</html>