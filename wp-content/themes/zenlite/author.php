<?php get_header();

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
if( !is_paged() ) :?>
<div <?php post_class(); ?>>
<h1 class="post-title"><?php _e('About', 'zenlite');?> <?php echo $curauth->display_name; ?></h1>
<dl class="author-details">

<?php if( $curauth->user_description !='' ) :?>
<dt class="bio"><?php _e('Bio', 'zenlite');?>:</dt>
<dd class="bio"><?php echo $curauth->user_description; ?></dd>
<?php endif;?>

<?php if( $curauth->user_url !='' ) :?>
<dt class="website"><?php _e('Website', 'zenlite');?>:</dt>
<dd class="website"><a href="<?php echo $curauth->user_url;?>"><?php echo $curauth->user_url;?></a></dd>
<?php endif;?>

<?php if(function_exists('get_the_author_meta')) :?>
<?php if( get_the_author_meta('jabber', $curauth->ID) != '') :?>
<dt class="jabber"><?php _e('Jabber / Google Talk', 'zenlite');?>:</dt>
<dd class="jabber"><?php the_author_meta('jabber', $curauth->ID);?></dd>
<?php endif;?>

<?php if( get_the_author_meta('aim', $curauth->ID) != '') :?>
<dt class="aim"><?php printf( __('<abbr title="%1$s">%2$s</abbr>', 'zenlite') ,  __('AOL Instant Messenger', 'zenlite'), __('AIM', 'zenlite'));?>:</dt>
<dd class="aim"><?php the_author_meta('aim', $curauth->ID);?></dd>
<?php endif;?>

<?php if( get_the_author_meta('yim', $curauth->ID) != '') :?>
<dt class="yim"><?php printf( __('%1$s <abbr title="%2$s">%3$s</abbr>', 'zenlite') ,  __('Yahoo', 'zenlite'),  __('Instant Messenger', 'zenlite'), __('IM', 'zenlite'));?>:</dt>
<dd class="yim"><?php the_author_meta('yim', $curauth->ID);?></dd>
<?php endif;?>

<?php endif;?>
</dl></div>
<?php endif;?>

<h1 class="posts-by"><?php _e('Posts by', 'zenlite');?> <?php echo $curauth->display_name; ?></h1>

<?php get_template_part( 'loop', 'index' );

get_footer();