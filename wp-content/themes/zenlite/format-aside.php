<h2 class="post-title"><?php if( !is_single() ) :?><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute( array('before' => __('Permalink to ', 'zenlite'), 'after' => '') ); ?>"><?php endif;?><?php the_title();?><?php if( !is_single() ) :?></a><?php endif;?></h2>

<?php if( !is_search() ) :?>
<ul class="meta posthead">
<li><?php edit_post_link(sprintf( __('Edit %1$s', 'zenlite'), get_the_title() ) ); ?></li>
</ul>
<?php endif;?>

<div class="postcontent">
<?php
if( !is_single() ):
if(has_post_thumbnail() ) : the_thumbnail();
 else : ?><img class="wp-post-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/aside.png" width="100" height="100" alt="" />
 <?php endif;?>
<?php the_excerpt();
else: the_content();
endif;?>
</div>

<ul class="meta postfoot">

<?php if( !is_single() ) :?><li class="more-link"><?php echo zenlite_author_display('', __(' @ ', 'zenlite') );?><?php if( !is_single() ):?><a href="<?php the_permalink();?>" title="<?php the_title_attribute( array('before' => 'Permalink to: ', 'after' => '') ); ?>"><?php endif;?><?php the_time(get_option('date_format')); ?> <?php the_time(get_option('time_format'));?><?php if( !is_single() ):?></a><?php endif;?></li><?php endif;?>

<?php if( !is_single() ):?>
<?php if( 'open' == $post->comment_status) : ?>
<li class="comment-link"><?php comments_popup_link(
__('Comment on ', 'zenlite') . get_the_title($id),
__('1 Comment on ', 'zenlite') . get_the_title($id),
 __('% Comments on ', 'zenlite') . get_the_title($id),
'postcomment',
__('Comments are off for ', 'zenlite') . get_the_title($id)
); ?> &raquo;</li><?php endif;?>

<?php else :?><li><?php echo zenlite_author_display('', __(' @ ', 'zenlite') );?><?php the_time(get_option('date_format')); the_time(get_option('time_format'));?></li>
<?php endif;?>

<?php if(!is_category() && !is_search() && get_the_category() ) :?><li class="cats"><?php _e('Filed under:', 'zenlite');?> <ul><li><?php the_category(',</li> <li>') ?></li></ul></li><?php endif;?>

<?php if( !is_tag() && !is_search() && get_the_tag_list() ) :?><li class="tags"><?php _e('Tags:', 'zenlite');?> <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>

</ul>

<?php comments_template();