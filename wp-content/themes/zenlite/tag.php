<?php
get_header();

if( !is_paged() ) wp_tag_cloud('format=list&unit=em&largest=2.5&smallest=.9em&number=0');

get_template_part( 'loop', 'tag' );

get_footer();