<?php get_header();?>


<h1 class="post-title"><?php _e('Search Results', 'zenlite');?></h1>

<?php get_search_form();?>

<?php
$zenlite_tot_pages = $wp_query->max_num_pages;
if($zenlite_tot_pages ==1) $zenlite_tot_pages.= __(' page', 'zenlite');
else $zenlite_tot_pages .= __(' pages', 'zenlite');
$zenlite_curr_page = $paged;
if($zenlite_curr_page =='') $zenlite_curr_page = 1;
$zenlite_searchterm = trim(get_search_query());?>
<p><?php _e('You searched for', 'zenlite');?> <em><?php echo $zenlite_searchterm;?></em>.<br />

<?php if (have_posts()) : ?>
<?php _e('Displaying page', 'zenlite');?> <?php echo $zenlite_curr_page;?> <?php _e('of', 'zenlite');?> <?php echo $zenlite_tot_pages;?> <?php _e('of results', 'zenlite');?>:</p>

<ul class="result-list">

<?php while (have_posts()) : the_post(); ?>
<li <?php post_class();?>><?php
$format = get_post_format();
if( $post->post_type == 'page' ) $format = 'page';
elseif ( false === $format ) $format = 'standard';
get_template_part( 'format', $format );
?></li>
<?php endwhile; ?>
</ul>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts', 'zenlite') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts', 'zenlite') );?></li>
</ul>

<?php else : ?>
<span class="sorry"><?php printf( __('Sorry - I couldn\'t find anything on %1$s%2$s%3$s', 'zenlite'), '<span>', $zenlite_searchterm, '</span>');?></span></p>
<?php endif; ?>

<?php get_footer();