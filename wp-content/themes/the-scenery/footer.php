<!-- begin footer -->
			</div><!--end of the body div-->
			<?php get_sidebar(); ?>
		</div><!--end of the content div-->
                <?php if(thescenery_getsetting('pageList', 'true') == 'true'){?>
		    <div id="footer-menu" class="dropdown-menu <?php echo is_search() ? "current_not_selecting" : "current_selecting";?>">
			<ul>
			    <li id="footer-menu-leftconner">&nbsp;</li>
			    <li class="page_item <?php echo is_home() ? "current_page_item" : (!is_page() && !is_search() ? "current_page_ancestor" : "")?>"><a title="<?php _e('Home')?>" href="<?php bloginfo("url")?>"><?php _e('Home')?></a></li>
			    <?php wp_list_pages(array(
				"title_li" => ""
			    )) ?>
			    <li id="footer-menu-rightconner">&nbsp;</li>
			</ul>
		    </div>
                <?php }?>
                <?php if(thescenery_getsetting('searchBox', 'true') ==  'true'){?>
                <div id="footer-searchform-left">
                        <form id="footer-searchform" method="get" action="<?php bloginfo('url'); ?>" class="<?php if(is_search()){ ?>current_searching current_selecting<?php } else {?>current_not_selecting<?php }?>">
                                <div id="footer-searchform-leftconner">&nbsp;</div>
                                <div id="footer-searchform-conent">        
                                        <input name="s" id="s" size="15" type="text" <?php if( function_exists("is_search") && is_search()) { echo 'value="';the_search_query(); echo'"'; } ?>><input value="<?php _e("Search")?>" type="submit">
                                        <?php if(is_search()){ ?><script type="text/javascript">var s=jQuery("#s")[0];s.focus();s.select();</script><?php } ?>
                                </div>
                        </form>
                </div>
                <?php }?>
		<div id="body-close">&nbsp;</div>
		<div id="footer">
			<div id="footer-shadow"></div>
			<div id="footer-body">
				<div class="credit"><!--<?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. --> <cite><?php echo sprintf(__("Powered by <a href='http://wordpress.org/' title='%s'><strong>WordPress</strong></a>"), __("Powered by WordPress, state-of-the-art semantic personal publishing platform.")); ?>. <?php _e('Current Theme')?>: <a href="http://leen.name/the-scenery-wp-theme" target="_blank">The Scenery</a>.</cite></div>
				<?php wp_footer(); ?>
			</div>
		</div>
</div><!--end of non-background-->
<script type="text/javascript">
function replaceFooter(){
	if(document.getElementById('non-background').scrollHeight <= document.documentElement.clientHeight)
		jQuery("#footer").prependTo("body").css({position: "absolute",bottom: 0, 'z-index': 3});
	else
		jQuery("#footer").appendTo("#non-background").css("position", "relative");
}
if(jQuery.browser.safari)//there are bugs here for webkit
	jQuery(function(){
		setTimeout(replaceFooter, 100);
	});
else
	replaceFooter();
jQuery(window).resize(replaceFooter);
</script>

<style type="text/css">
body{
  background-repeat: no-repeat;
  background-attachment: fixed;
  _behavior: url(<?php bloginfo('template_url'); ?>/csshover.htc);/*damn IE6*/
}
</style>

<div id="background">
<!--flash background-->
<script language="JavaScript" type="text/javascript">
<!--
// function to pass settings to flex
function getTheSceneryThemeSettings(){
        return {
        <?php
                foreach(thescenery_getsettings() as $key => $value){
                        ?>
                                <?php echo $key?>: '<?php echo $value?>',
                        <?php
                }
        ?>
          _:''
        };
}
var backgroundImageWidth, backgroundImageHeight;
var backgroundImageLoaded = false;
function onBackgroundImageLoad(){
  // http://stackoverflow.com/questions/318630/get-real-image-width-and-height-with-javascript-in-safari-chrome
  if(backgroundImageLoaded)
	return;
  if (jQuery.browser.safari && document.readyState != "complete"){
    setTimeout( arguments.callee, 100 );
    return;
  }
  backgroundImageLoaded = true;
  backgroundImageWidth = jQuery('#backgroundImage').innerWidth();
  backgroundImageHeight = jQuery('#backgroundImage').innerHeight();
  if(backgroundImageWidth > 0 && backgroundImageHeight > 0){
    updateBackgroundImage();
    jQuery('#backgroundImage').css('visibility', 'visible');
    jQuery(window).resize(updateBackgroundImage);
  }
}
function updateBackgroundImage(){
  var settings = getTheSceneryThemeSettings();
  var windowWidth = jQuery(window).width(), windowHeight = jQuery(window).height();
  var width, height;
  var top = 0, left = 0;
  switch(settings.backgroundType){
    case 'stretch':
      width = windowWidth;
      height = windowHeight;
      break;
    case 'keepratio':
      if(windowHeight > 0 && windowWidth > 0){
        if(windowWidth / windowHeight > backgroundImageWidth / backgroundImageHeight){
          // do width fit
          width = windowWidth;
          height = windowWidth * backgroundImageHeight / backgroundImageWidth;
          
          if(settings.backgroundVerticalAlign == 'top')
            top = 0;
          else if(settings.backgroundVerticalAlign == 'center')
            top = (windowHeight - height) / 2;
          else
            top = windowHeight - height;
          
          
        }else{
          // do height fit
          height = windowHeight;
          width = windowHeight * backgroundImageWidth / backgroundImageHeight;
          
          
          if(settings.backgroundHorizontalAlign == 'left')
            left = 0;
          else if(settings.backgroundHorizontalAlign == 'center')
            left = (windowWidth - width) / 2;
          else
            left = windowWidth - width;
        }
      }
      break;
  }
  
  jQuery('#backgroundImage').css({
    'width': width + 'px',
    'height': height + 'px',
    'top': top + 'px',
    'left': left + 'px'
  });
}
jQuery(function(){
  var settings = getTheSceneryThemeSettings();
  if(settings.backgroundType == 'original')
    jQuery("body").css({
      "background-image": "url(<?php echo thescenery_getsetting('backgroundImage')?>)",
      "background-position": settings.backgroundVerticalAlign + " " + settings.backgroundHorizontalAlign
    });
  else {
    jQuery('<img id="backgroundImage" src="<?php echo htmlspecialchars(thescenery_getsetting('backgroundImage'))?>" onload="onBackgroundImageLoad();" style="position: absolute; visibility: hidden;" />').appendTo("#background");
	if(jQuery.browser.safari)
		setTimeout(onBackgroundImageLoad, 100);
  }
});
// -->
</script>

<noscript>
  <!--if there is no javascript, fallback to css background-->
  <style type="text/css">
  body{
    background-image:url(<?php echo thescenery_getsetting('backgroundImage')?>);
    background-repeat: no-repeat;
    background-attachment: fixed;
  }
  </style>
</noscript>
		
	</div>
</body>
</html>