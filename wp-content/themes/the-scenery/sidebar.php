
<!-- begin sidebar -->
<ul id="sidebar1">
<?php 	/* Widgetized sidebar, if you have the plugin installed. */
if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>

<?php wp_list_categories('show_count=1&title_li=<h3>' . __('Categories') . '</h3>'); ?>

<li id="archives"><h3><?php _e('Archives')?></h3>
        <ul>
        <?php wp_get_archives('type=monthly'); ?>
        </ul>
</li>

<?php /* If this is the frontpage */ if ( is_home() || is_page() ) { ?>
        <?php wp_list_bookmarks('title_before=<h3>&title_after=</h3>'); ?>

        <li id="meta"><h3>Meta</h3>
        <ul>
                <?php wp_register(); ?>
                <li><?php wp_loginout(); ?></li>
                <li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
                <li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
                <li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
                <?php wp_meta(); ?>
        </ul>
        </li>
<?php } ?>

        <script type="text/javascript">
          jQuery('#sidebar1>li').each(function(){
            jQuery(this).addClass('widget_' + jQuery(this).attr('id') + ' ' + 'widget_' + this.className + ' ' + 'widget');
          });
        </script>
<?php endif; ?>
</ul>
<script type="text/javascript">
if(!jQuery.browser.msie || jQuery.browser.version >= 7){
    function trimSlashes(s){
        while(s[s.length-1] == '/')s=s.substring(0,s.length-1);
        return s;
    }
    var currentUrl = trimSlashes(window.location.href);
    jQuery("#archives a[href]").each(function(){
        if(trimSlashes(jQuery(this).attr("href")) == currentUrl)
            jQuery(this).parent("li").addClass("current-archive");
    });
    jQuery("#calendar a[href]").each(function(){
        if(trimSlashes(jQuery(this).attr("href")) == currentUrl)
            jQuery(this).parent("td").addClass("current-archive");
    });
}
</script>
<script type="text/javascript">
var pageType = '_unknown';
<?php
$page_types = array("category", "search", "author", "tag", "day", "month", "archive", "page");
$page_classes = array(
    "archive" => "widget_archive",
    "search" => "widget_search",
    "author" => "widget_authors",
    "tag" => "widget_tag_cloud",
    "category" => "widget_categories",
    "page" => "widget_pages",
    "day" => "widget_calendar",
    "month" => "widget_calendar|widget_archive"
);
foreach($page_types as $page_type)
    if(function_exists("is_$page_type") && call_user_func("is_$page_type")):?>
        pageType = "<?php echo $page_classes[$page_type];?>";
<?php
        break;
    endif;
?>
// jQuery.animate runs very slow on IE, we use jQuery.css as a replacement
jQuery.prototype.slidebarAnimate = jQuery.browser.msie ? jQuery.prototype.css : jQuery.prototype.animate;
var hideFunction = function(){
    jQuery(">*:not(h3:first)", this).stop().slidebarAnimate({"opacity": 0});
    jQuery(this).stop().slidebarAnimate({"opacity": 0.4}).removeClass("widget-hover");
};
var faseHideFunction = function(){
    jQuery(">*:not(h3:first)", this).css("opacity", 0);
    jQuery(this).css("opacity", 0.4).removeClass("widget-hover");
};
var showFunction = function(){
    jQuery(">*:not(h3:first)", this).stop().slidebarAnimate({"opacity": 1});
    jQuery(this).stop().slidebarAnimate({"opacity": 0.6}).addClass("widget-hover");
};
var initialShowFunction = function(){
    jQuery(">*:not(h3:first)", this).css("opacity", 1);
    jQuery(this).css("opacity", 0.6).addClass("widget-hover");        
};
var jqWidgets = jQuery("#sidebar1>.widget");
var i=jqWidgets.length;
if(!jQuery.browser.msie || jQuery.browser.version >= 7)
    jqWidgets.each(function(){
        if(!jQuery(this).attr("class").match("(" + pageType + ")")){
            if(fromWordpress)
                faseHideFunction.apply(this);
            else{
                var me=this;
                setTimeout(function(){
                    hideFunction.apply(me);
                },  200 * (i--));
            }
            jQuery(this).hover(showFunction, hideFunction);
            jQuery("a,input,button",this).focus(function(){
                showFunction.apply(jQuery(this).parents(".widget")[0]);
            }).blur(function(){
                hideFunction.apply(jQuery(this).parents(".widget")[0]);        
            });
        }else
            initialShowFunction.apply(this);
    });
</script>


<script type="text/javascript">
var removeHandle4IE = undefined;
var addHandle4IE = undefined;
if(!jQuery.browser.msie || jQuery.browser.version >= 7)
    jQuery("#sidebar1 ul li").each(function(){
        var me = this;
        if(jQuery("a", this).length == 1 && jQuery("a", this).attr("href") && !jQuery("a", this).attr("onclick"))
            jQuery(this).addClass("li-with-link").click(function(e){
                if((e.target || e.srcElement) != me) return ;
                window.location.href = jQuery("a", this).attr("href");
            }).mouseover(function(e){
                if(jQuery.browser.msie && !addHandle4IE){
                    addHandle4IE = setTimeout(function(){
                        if(!jQuery(me).hasClass("li-hover-4ie"))
                            jQuery(me).addClass("li-hover-4ie");
                        addHandle4IE = undefined;
                    }, 100);
                    removeHandle4IE = undefined;
                }
                window.status = jQuery("a", this).attr("href"); 
            }).mouseout(function(e){
                if(jQuery.browser.msie && !removeHandle4IE){
                    removeHandle4IE = setTimeout(function(){
                        if(jQuery(me).hasClass("li-hover-4ie"))
                            jQuery(me).removeClass("li-hover-4ie");
                        removeHandle4IE = undefined;
                    }, 100);
                    addHandle4IE = undefined;
                }
                window.status = '';
            });
    });
</script>
<!-- end sidebar -->
