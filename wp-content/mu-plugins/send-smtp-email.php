<?php
/**
 * Plugin Name: A/I Send SMTP email
 * Plugin URI: https://noblogs.org
 * Description: Sends mail via SMTP instead than PHP mail function
 * Author: Autistici/Inventati
 * Author URI: https://autistici.org
 * Version: 1.0
 */
 
// Put your code snippets below this line.

/**
 * This function will connect wp_mail to a (possibly authenticated)
 * external SMTP server, instead of using the PHP mail() function.
 * 
 * Values are constants set in wp-config.php
 */
if (defined('SMTP_HOST')) {
	add_action('phpmailer_init', 'send_smtp_email');
	function send_smtp_email($phpmailer) {
		$phpmailer->isSMTP();
		$phpmailer->Host       = SMTP_HOST;
		$phpmailer->SMTPAuth   = SMTP_AUTH;
		$phpmailer->Port       = SMTP_PORT;
		$phpmailer->Username   = SMTP_USER;
		$phpmailer->Password   = SMTP_PASS;
		$phpmailer->SMTPSecure = SMTP_SECURE;
		$phpmailer->From       = SMTP_FROM;
		$phpmailer->FromName   = SMTP_NAME;
		// Do not attempt STARTTLS even if the
		// server offers it - use SMTPSecure instead.
		$phpmailer->SMTPAutoTLS = false;
	}
}

