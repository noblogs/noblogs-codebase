<?php

// wp-nginx-map.php
//
// Stampa una mappa delle assegnazioni blog -> backend, per NGINX.
// IP interno del master.
$master_url = file_get_contents('/etc/noblogs/master');
$noblogs_master = parse_url($master_url)['host'];

// Load wordpress api.
define('WP_CACHE',false);
require_once(__DIR__ . '../wp/wp-load.php');

// Port that the noblogs apache2 instance is listening on (as a string).
define('NOBLOGS_PORT','92');

// Return all blogs.
function get_blogs() {
  global $wpdb;
  $sql = "SELECT blog_id, domain FROM $wpdb->blogs WHERE deleted = 0 AND archived = '0' ORDER BY domain ASC";
  $result = $wpdb->get_results($sql);
  return ($result);
}

function printline($s) {
  echo $s . "\n";
}

function backend_to_http_endpoint($backend) {
  if (substr($backend, 0, 8) != 'backend_') {
    error_log('diamine, di questo backend non so che farmene: ' . $backend);
    return $noblogs_master . ':' . NOBLOGS_PORT;
  }
  $id = substr($backend, 8);
  return '172.16.1.' . $id . ':' . NOBLOGS_PORT;
}

// Print the blog -> backend map.
function generate_backend_map($blogs) {
  global $wpdb;
  global $noblogs_master;
  $wpdb_hash = &$wpdb->hash_map;

  printline('map $http_host $backend_noblogs {');
  printline(' default http://' . $noblogs_master . ':' . NOBLOGS_PORT . ';');
  foreach ($blogs as $blog) {
    $blog_id = $blog->blog_id;
    if ($blog_id == 1)
      continue;
    $backend_id = $wpdb_hash->lookup($blog_id);
    $backend_http = backend_to_http_endpoint($backend_id);
    printline(' ' . $blog->domain . ' http://' . $backend_http . ';');
  }
  printline('}');
}

// Print the blog -> site map.
// This is used to send apache the right site name for logging purposes.
function generate_site_map($blogs) {
  printline('map $http_host $site_noblogs {');
  printline(' default noblogs.org;');
  foreach ($blogs as $blog) {
    $blog_id = $blog->blog_id;
    if ($blog_id == 1)
      continue;
    printline(' ' . $blog->domain . ' ' . $blog->domain . ';');
  }
  printline('}');
}

function generate_maps() {
  $all_blogs = get_blogs();
  generate_backend_map($all_blogs);
  generate_site_map($all_blogs);
}

generate_maps();
