<?php

// wp-nginx-map.php (JSON version)

// Load wordpress api.
define('WP_CACHE',false);
require_once(__DIR__ . '../wp/wp-load.php');

// Return all blogs.
function get_blogs() {
  global $wpdb;
  $sql = "SELECT blog_id, domain FROM $wpdb->blogs WHERE deleted = 0 AND archived = '0' ORDER BY domain ASC";
  $result = $wpdb->get_results($sql);
  return ($result);
}

function backend_to_shard_id($backend) {
  if (substr($backend, 0, 8) != 'backend_') {
    error_log('diamine, di questo backend non so che farmene: ' . $backend);
    return '0';
  }
  return substr($backend, 8);
}

// Print the blog -> shard_id map.
function generate_shard_map($blogs) {
  global $wpdb;
  $wpdb_hash = &$wpdb->hash_map;

  $shard_map = array();
  foreach ($blogs as $blog) {
    $blog_id = $blog->blog_id;
    if ($blog_id == 1)
      continue;
    $backend_id = $wpdb_hash->lookup($blog_id);
    $shard_id = backend_to_shard_id($backend_id);
    $shard_map[$blog->domain] = $shard_id;
  }
  echo json_encode($shard_map);
}

function generate_maps() {
  $all_blogs = get_blogs();
  generate_shard_map($all_blogs);
}

generate_maps();
